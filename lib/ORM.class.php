<?php

include_once '../configuration.php';
require_once 'DB.class.php';

class ORM {

  private $__entity_name;


  //public methods
  public function __construct ($entityName) {
    if (defined('DB_CONNECTION')) {
      $this->__dbdata = unserialize(DB_CONNECTION);

      if (is_array($this->__dbdata)) {
        if (!array_key_exists('server', $this->__dbdata)) {
          throw new Exception('server not defined.');
          exit();
        }
        if (!array_key_exists('username', $this->__dbdata)) {
          throw new Exception('username not defined.');
          exit();
        }
        if (!array_key_exists('password', $this->__dbdata)) {
          throw new Exception('password not defined.');
          exit();
        }
        if (!array_key_exists('database', $this->__dbdata)) {
          throw new Exception('database not defined.');
          exit();
        }
        if (!array_key_exists('encoding', $this->__dbdata)) {
          throw new Exception('encoding not defined.');
          exit();
        }
        return;
      }
      else {
        throw new Exception('DB_CONECTION is invalid.');
        exit();
      }
    }
    else {
      throw new Exception('DB_CONECTION not defined.');
      exit();
    }

    $this->__entity_name = $entityName;
    $this->createEntity($entityName);
  }

  public function save () {
    //get all the column values
    //build query
    //save and set id if autoincrement
  }

  //load function requires sigle primary key in a table.
  public function load (mixed $primary) {
    $database = new DB();
    $primary_key_column;

    //single primary key requirement
    foreach ($this->__columns as $column) {
      if ($column['COLUMN_KEY'] == 'PRI') {
        $primary_key_column = $column;
        break;
      }
    }

    if ($primary_key_column) {
      $query = sprintf('');
    }

    $database->Query($strQuery);
  }

  public function delete () {
    throw new Exception('Not implemented yet.');
    //delete from database
  }





  /**
   *
   * @param array $condition_array list of columns to be included in the where condition
   * values are preset in the properties
   */
  private function BuildSelectQuery ($condition_array) {
    //get all columns escaped with ` and separated by ,
    $columns = implode(',', array_walk(array_keys($this->__columns), array($this, 'EscapeColumn')));

    $condition_columns =

    foreach ($condition_array as &$condition) {
      $condition= $this->EscapeColumn($condition, null) . '';
    }



    $query = sprintf('SELECT %s
                      FROM %s
                      WHERE %s =  ', $columns, $this->__entity_name, $this->__primary_key, $this->CastToDBValue($this->{$this->__primary_key}));
  }

  private function EscapeColumn (&$item1, $key, $escape_char = '`') {
    $item1 = "$escape_char$item1$escape_char";
  }

  /* varchar    |
    | bigint     |
    | longtext   |
    | datetime   |
    | int        |
    | decimal    |
    | text
    | tinyint
    | timestamp
    | mediumtext
    | double
    | longblob
    | date
    | char
    | binary
    | smallint
    | enum
    | blob
    | float
   */

  public function CastToDBValue ($value, $columnname) {

    $column_info = $this->__columns[$columnname];

    if ($column_info && array_key_exists('DATA_TYPE', $column_info)) {

      switch (strtoupper($column_info['DATA_TYPE'])) {

        //numeric datatypes
        case 'INTEGER':
        case 'INT':
        case 'SMALLINT':
        case 'TINYINT':
        case 'MEDIUMINT':
        case 'BIGINT':
        case 'BIT':
        case 'DECIMAL':
        case 'NUMERIC':
        case 'FLOAT':
        case 'DOUBLE':
          return $value;

        case 'DATE':
        case 'DATETIME':
        case 'TIME':
          if (gettype($value) == 'object' && $value instanceof DateTime) {
            return new QDateTime($this->strColumnArray[$strColumnName]);
          }
          else {
            return (string) $value;
          }

        //all string type values must escaped with single quotes
        default:
          return "'$value'";
      }
    }
  }

}

?>