<?php

include_once '../configuration.php';
require_once 'DB.class.php';

/**
 * This is a generic class to describe the database entity and to hold its values
 */
class Entity {

   private $__entity_name;
   private $__columns;

  //public methods
  public function __construct ($entityName) {
    if (defined('DB_CONNECTION')) {
      $this->__dbdata = unserialize(DB_CONNECTION);

      if (is_array($this->__dbdata)) {
        if (!array_key_exists('server', $this->__dbdata)) {
          throw new Exception('server not defined.');
          exit();
        }
        if (!array_key_exists('username', $this->__dbdata)) {
          throw new Exception('username not defined.');
          exit();
        }
        if (!array_key_exists('password', $this->__dbdata)) {
          throw new Exception('password not defined.');
          exit();
        }
        if (!array_key_exists('database', $this->__dbdata)) {
          throw new Exception('database not defined.');
          exit();
        }
        if (!array_key_exists('encoding', $this->__dbdata)) {
          throw new Exception('encoding not defined.');
          exit();
        }
        return;
      }
      else {
        throw new Exception('DB_CONECTION is invalid.');
        exit();
      }
    }
    else {
      throw new Exception('DB_CONECTION not defined.');
      exit();
    }

    $this->__entity_name = $entityName;
    $this->createEntity($entityName);
  }


  // MAGIC methods
  public function __get ($property) {
    if (!array_key_exists($this->__columns, $property)) {
      return null;
    }
    return $this->{$property};
  }

  public function __set ($property, $value) {
    if (array_key_exists($property, $this->__columns)) {
      $this->{$property} = $value;
    }
  }

  //private methods
  private function createEntity ($entityName) {

    $database = new DB();
    $database->Connect();
    $query = sprintf("select *
                      from information_schema.columns
                      where table_schema='%s' and
                      table_name = '%s'", $this->dbData['database'], $entityName);

    $result = $database->Query($query);
    foreach ($result as $row) {
      $this->__columns[$row['column_name']] = $row;
    }
  }
}
?>